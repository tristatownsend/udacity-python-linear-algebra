from math import sqrt

class Vector(object):
    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple(coordinates)
            self.dimension = len(coordinates)

        except ValueError:
            raise ValueError('The coordinates must be nonempty')

        except TypeError:
            raise TypeError('The coordinates must be an iterable')

    def plus (self, v):
        new_coordinates = [x+y for x,y in zip(self.coordinates, v.coordinates)]
        return Vector(new_coordinates)
    
    def minus (self, v):
        new_coordinates = [x-y for x,y in zip(self.coordinates, v.coordinates)]
        return Vector(new_coordinates)

    def calc_scalar (self, c):
        new_coordinates = [c*x for x in self.coordinates]
        return Vector(new_coordinates)

    def calc_magnitude (self):
        magnitude = [x ** 2 for x in self.coordinates]
        # result = 0
        # for i in magnitude:
        #     result += i
        # result = math.sqrt(result)
        # return result
        return sqrt(sum(magnitude))
    
    def calc_direction (self):
        magnitude = self.calc_magnitude()
        # inverse_mag = 1/magnitude
        # direction = [inverse_mag * x for x in self.coordinates]
        return self.calc_scalar(1./magnitude) 

    def __str__(self):
        return 'Vector: {}'.format(self.coordinates)


    def __eq__(self, v):
        return self.coordinates == v.coordinates

# v = Vector([8.218, -9.341])
# w = Vector([-1.129, 2.111])  
# print(v.plus(w))

# v = Vector([7.119, 8.215])
# w = Vector([-8.223, 0.878])
# print(v.minus(w))

# v = Vector([1.671, -1.012, -0.318])
# c = 7.41
# print(v.calc_scalar(c))

# v = Vector([-0.221, 7.437])
# v = Vector([8.813, -1.331, -6.247])
# print(v.calc_magnitude())

# v = Vector([5.581, -2.136])
v = Vector([1.996, 3.108, -4.554])
print(v.calc_direction())
